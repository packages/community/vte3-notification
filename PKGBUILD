# Maintainer: Stefano Capitani <stefano@manjaro.org>
# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Maintainer: Mark Wagie <mark@manjaro.org>
# Contributor: Jason Edson <jaysonedson@gmail.com>
# Contributor: Davi da Silva Böger <dsboger@gmail.com>
# Contributor: Manuel Hüsers <manuel.huesers@uni-ol.de>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgbase=vte3-notification
pkgname=(
  vte-notification-common
  vte3-notification
  vte4-notification
)
pkgver=0.76.4
pkgrel=2
pkgdesc="Virtual Terminal Emulator widget with Fedora patches"
arch=('x86_64' 'aarch64')
url="https://wiki.gnome.org/Apps/Terminal/VTE"
license=(GPL-3.0-or-later AND LGPL-3.0-or-later AND MIT AND X11 AND CC-BY-4.0)
depends=(
  cairo
  fribidi
  gcc-libs
  gdk-pixbuf2
  glib2
  glibc
  gnutls
  icu
  lz4
  pango
  pcre2
  systemd
  systemd-libs
)
makedepends=(
  at-spi2-core
  gi-docgen
  git
  glib2-devel
  gobject-introspection
  gperf
  gtk3
  gtk4
  meson
  vala
)
options=(!lto)

# Fedora patches: https://src.fedoraproject.org/rpms/vte291/tree/
_frepourl='https://src.fedoraproject.org/rpms/vte291'
_frepobranch='rawhide'
_fpatchfile0='0001-a11y-implement-GtkAccessibleText.patch'
_fpatchfile1='0001-add-notification-and-shell-precmd-preexec.patch'
_fcommit='20f805f6a3d1769d603d2862ab24ed5ed1ddb6ad'

source=(
  "git+https://gitlab.gnome.org/GNOME/vte.git#tag=$pkgver"
  "${_fpatchfile0}-${_fcommit}::${_frepourl}/raw/${_fcommit}/f/${_fpatchfile0}"
  "${_fpatchfile1}-${_fcommit}::${_frepourl}/raw/${_fcommit}/f/${_fpatchfile1}")
sha256sums=('628e9202b3578a825e6a135508c711a0ea0c236579f4ec1afe5d11f3feecce90'
            'e238d865e8a20a06d56b99f34a398ab3279d7228395a109e2a66bae32fddf760'
            '49374e5513735c4d31997709b6f9b9a444b0da5c04bc9c62d87919ab839aa1c3')

prepare () {
  cd vte
  patch -Np1 -i "../${_fpatchfile0}-${_fcommit}"
  patch -Np1 -i "../${_fpatchfile1}-${_fcommit}"
}

build() {
  local meson_options=(
    -D docs=false
  )

  arch-meson vte build "${meson_options[@]}"
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_vte-notification-common() {
  pkgdesc+=" (common files)"
  depends=(glibc)
  provides=("vte-common=${pkgver}")
  conflicts=('vte-common')

  meson install -C build --destdir "$pkgdir"

  cd "$pkgdir"

  _pick gtk3 usr/bin/vte-2.91
  _pick gtk3 usr/include/vte-2.91
  _pick gtk3 usr/lib/libvte-2.91.so*
  _pick gtk3 usr/lib/pkgconfig/vte-2.91.pc
  _pick gtk3 usr/lib/girepository-1.0/Vte-2.91.typelib
  _pick gtk3 usr/share/gir-1.0/Vte-2.91.gir
  _pick gtk3 usr/share/glade
  _pick gtk3 usr/share/vala/vapi/vte-2.91.{deps,vapi}

  _pick gtk4 usr/bin/vte-2.91-gtk4
  _pick gtk4 usr/include/vte-2.91-gtk4
  _pick gtk4 usr/lib/libvte-2.91-gtk4.so*
  _pick gtk4 usr/lib/pkgconfig/vte-2.91-gtk4.pc
  _pick gtk4 usr/lib/girepository-1.0/Vte-3.91.typelib
  _pick gtk4 usr/share/gir-1.0/Vte-3.91.gir
  _pick gtk4 usr/share/vala/vapi/vte-2.91-gtk4.{deps,vapi}
}

package_vte3-notification(){
  pkgdesc+=" (GTK3)"
  depends+=(
    at-spi2-core
    gtk3
    vte-notification-common
  )
  provides=("vte3=${pkgver}" 'libvte-2.91.so')
  conflicts=('vte3')

  mv gtk3/* "$pkgdir"
}

package_vte4-notification() {
  pkgdesc+=" (GTK4)"
  depends+=(
    gtk4
    vte-notification-common
  )
  provides+=("vte4=${pkgver}" libvte-2.91-gtk4.so)
  conflicts=(vte4)

  mv gtk4/* "$pkgdir"
}
